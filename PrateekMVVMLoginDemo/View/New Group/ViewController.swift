//
//  ViewController.swift
//  PrateekMVVMLoginDemo
//
//  Created by maximess206 on 20/03/20.
//  Copyright © 2020 maximess206. All rights reserved.
//

import UIKit
protocol ViewModelDelegate{
    func sendValue(from emailTextField: String?, passwordTextField: String?)
}
class ViewController: UIViewController{
    //MARK:Oulet Declaration
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var lblEmailValidate: UILabel!
    @IBOutlet weak var lblPasswordValidate: UILabel!
    //MARK:Variable Declaration
    var ViewModelAccess = ViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        ViewModelAccess.delegate = self
        self.setupValidationLabels(isHidden: true)
    }
    //MARK:IBActions
    @IBAction func login(_ sender: UIButton) {
        ViewModelAccess.sendValue(from: txtEmail.text, passwordTextField: txtPassword.text)
    }
    
    @IBAction func ForgotPasswordBtnClicked(_ sender: UIButton) {
        let vc = storyboard!.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
             self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func signUpBtnClicked(_ sender: Any) {
        let vc = storyboard!.instantiateViewController(withIdentifier: "RegisterUserViewController") as! RegisterUserViewController
               self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK:- Functions
    func setupValidationLabels(isHidden: Bool) {
        self.lblEmailValidate.isHidden = isHidden
        self.lblPasswordValidate.isHidden = isHidden
    }

}
extension ViewController : ViewControllerDelegate{
    func getInfoBackEmail(isemailValid: Bool?) {
        self.lblEmailValidate.isHidden = isemailValid!
    }
    func getInfoBackPassword(ispasswordValid: Bool?) {
        self.lblPasswordValidate.isHidden = ispasswordValid!
    }
    func sendStatus(isValid: Bool?) {
        print("status is :\(isValid)")
        showAlert(withTitle:"success", withMessage: "Login Successfull")
    }
}
