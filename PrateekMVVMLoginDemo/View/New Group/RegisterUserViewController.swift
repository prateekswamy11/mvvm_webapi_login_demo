//
//  RegisterUserViewController.swift
//  PrateekMVVMLoginDemo
//
//  Created by maximess206 on 20/03/20.
//  Copyright © 2020 maximess206. All rights reserved.
//

import UIKit
protocol ViewModelDelegateRegisterUser{
    func sendValue(nameTextField: String?,emailTextField: String?, passwordTextField: String?,dobTextField: String?)
}
class RegisterUserViewController: UIViewController,UITextFieldDelegate {
       //MARK:- Outlets
        @IBOutlet weak var txtName: UITextField!
        @IBOutlet weak var lblNameValidationMsg: UILabel!
        @IBOutlet weak var txtEmail: UITextField!
        @IBOutlet weak var lblEmailValidationMsg: UILabel!
        @IBOutlet weak var txtPswd: UITextField!
        @IBOutlet weak var lblPswdValidationMsg: UILabel!
        @IBOutlet weak var lblDOBValidationMsg: UILabel!
        @IBOutlet weak var txtDOB: UITextField!
        @IBOutlet weak var toolBarForDOB: UIToolbar!
        @IBOutlet weak var dtPickerForDOB: UIDatePicker!
        //MARK:Variable Declaration
        var ViewModelAccess = ViewModelRegisterUser()
        //MARK:- View delegates
        override func viewDidLoad() {
            super.viewDidLoad()
            self.ViewModelAccess.delegate = self
            self.initialSetup()
            self.setupValidationLbls(isHidden: true)
        }
        
        //MARK:- Actions
        @IBAction func registerBtnClicked(_ sender: UIButton) {
            ViewModelAccess.sendValue(nameTextField: self.txtName.text!, emailTextField: self.txtEmail.text!, passwordTextField: self.txtPswd.text!, dobTextField: self.txtDOB.text!)
        }
        
        @IBAction func btnToolBarDoneClicked(_ sender: UIBarButtonItem) {
            self.toolBarForDOB.isHidden = true
            self.dtPickerForDOB.isHidden = true
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.medium
            let strDate = dateFormatter.string(from: self.dtPickerForDOB.date)
            self.txtDOB.text = strDate
        }
        
        //MARK:- Functions
        func initialSetup() {
            self.dtPickerForDOB.backgroundColor = .white
            self.dtPickerForDOB.maximumDate = Date()
        }
        
        func setupValidationLbls(isHidden: Bool) {
            self.lblEmailValidationMsg.isHidden = isHidden
            self.lblNameValidationMsg.isHidden = isHidden
            self.lblPswdValidationMsg.isHidden = isHidden
            self.lblDOBValidationMsg.isHidden = isHidden
        }
    func textFieldDidBeginEditing(_ textField: UITextField) {
               self.view.endEditing(true)
               self.dtPickerForDOB.isHidden = false
               self.toolBarForDOB.isHidden = false
           }
    }

extension RegisterUserViewController: ViewControllerDelegateRegisterUser {
    func getInfoBackEmail(isemailValid: Bool?) {
        self.lblEmailValidationMsg.isHidden = isemailValid!
    }
    
    func getInfoBackPassword(ispasswordValid: Bool?) {
        self.lblPswdValidationMsg.isHidden = ispasswordValid!
    }
    
    func getInfoBackName(isNameValid: Bool?) {
        self.lblNameValidationMsg.isHidden = isNameValid!
    }
    
    func getInfoBackDob(isDobEmpty: Bool?) {
        self.lblDOBValidationMsg.isHidden = isDobEmpty!
    }
    
    func sendStatus(isValid: Bool?) {
         print("status is :\(isValid)")
        if isValid!{
            showAlert(withTitle:"success", withMessage: "Register Successfull")
        } else{
            showAlert(withTitle:"error", withMessage: "Check input")
        }
        
    }
    }
