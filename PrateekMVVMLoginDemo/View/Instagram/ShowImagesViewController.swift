//
//  ShowImagesViewController.swift
//  PrateekMVVMLoginDemo
//
//  Created by maximess206 on 26/03/20.
//  Copyright © 2020 maximess206. All rights reserved.
//

import UIKit
import Alamofire
import WebKit

class ShowImagesViewController: UIViewController {
   // MARK: - Variables
    var recieveAccessTokenFromLastVC : String?
    var arrayInstagramImages = [String]()
    var instagramImages = [UIImage]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAccessToken(code: recieveAccessTokenFromLastVC!)
        
        
    }
    //MARK: - User Defined Functions
    func  getImageFromUrl(imgURL:[String]) -> [UIImage] {
    for url in imgURL{
      let url = NSURL(string: url)
      let  data = NSData(contentsOf: url as! URL)
      let img = UIImage(data: data as!Data)
      self.instagramImages.append(img!)
     }
     return instagramImages
    }
    func getAccessToken(code: String){
        var access_token : String?
        var user_id : String?
        let _headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
        let params : Parameters =   [ "client_id":"\(INSTAGRAM_IDS.INSTAGRAM_CLIENT_ID)", "client_secret":"\(INSTAGRAM_IDS.INSTAGRAM_CLIENTSERCRET)", "grant_type":"authorization_code", "redirect_uri":"\(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI)", "code":"\(code)"]
        let url =  URL(string:"https://api.instagram.com/oauth/access_token" as String)
        
        AF.request(url!, method: .post, parameters: params, encoding: URLEncoding.httpBody , headers: _headers).responseJSON(completionHandler: {
            response in
            print("result----", response.value as Any)
            if response.value != nil{
                let jsonResponse = response.value as! NSDictionary
                
                if jsonResponse["access_token"] != nil
                {
                    access_token = String(describing: jsonResponse["access_token"]!)
                    print("access token.....\(access_token)")
                }
                
                if jsonResponse["user_id"] != nil{
                    user_id = String(describing: jsonResponse["user_id"]!)
                    print("user_id .........\(user_id)")
                }
                self.getMediaId(access_token: access_token!)
            }
            else if (response.error != nil){
                var error = response.error!.localizedDescription
                if error.contains("JSON could not") {
                    error = "Could not connect to server at the moment."
                }
            }
        })
    }
    
    func getMediaId(access_token: String){
           var mediaId : String?
           var caption : String?
           let _headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
           let url =  URL(string:"https://graph.instagram.com/me/media?fields=id,caption&access_token=\(access_token)" as String)
           
        AF.request(url!, method: .get, parameters: nil, encoding: URLEncoding.httpBody , headers: _headers).responseJSON(completionHandler: {
               response in
                if response.value != nil{
               print("result----", response.value as Any)
                    let fetchedData = response.value as! [String:Any]
                    print(fetchedData["data"])
                    if let getids = fetchedData["data"] as? [[String:Any]] {
                        for i in getids {
                            if let ids = i["id"] as? String {
                               // self.newids.append(ids)
                                self.getImagesUsingId(access_token: access_token, id: ids)
                            }
                        }
                        //print(self.newids)
                    }
               }
               else if (response.error != nil){
                   var error = response.error!.localizedDescription
                   if error.contains("JSON could not") {
                       error = "Could not connect to server at the moment."
                   }
               }
           })
       }
    func getImagesUsingId(access_token : String,id : String){
              var imageid : String?
              var imageUrl : String?
              let _headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
              let url =  URL(string:"https://graph.instagram.com/\(id)?fields=id,media_type,media_url,username,timestamp&access_token=\(access_token)" as String)
              AF.request(url!, method: .get, parameters: nil, encoding: URLEncoding.httpBody , headers: _headers).responseJSON(completionHandler: {
                  response in
                  print("result----", response.value as Any)
                  if response.value != nil{
                      let jsonResponse = response.value as! NSDictionary
                      
                      if jsonResponse["id"] != nil
                      {
                          imageid = String(describing: jsonResponse["id"]!)
                          print("imageid.....\(imageid)")
                      }
                      
                      if jsonResponse["media_url"] != nil{
                          imageUrl = String(describing: jsonResponse["media_url"]!)
                          print("imageurl .........\(imageUrl)")
                        self.arrayInstagramImages.append(imageUrl!)
                        
                        
                      }
                    self.getImageFromUrl(imgURL: self.arrayInstagramImages)
                  }
                  else if (response.error != nil){
                      var error = response.error!.localizedDescription
                      if error.contains("JSON could not") {
                          error = "Could not connect to server at the moment."
                      }
                  }
              })
    }
    
}
