//
//  InstagramViewController.swift
//  PrateekMVVMLoginDemo
//
//  Created by maximess206 on 23/03/20.
//  Copyright © 2020 maximess206. All rights reserved.
//

import UIKit
import Alamofire
import WebKit

class InstagramViewController: UIViewController,WKUIDelegate,WKNavigationDelegate {
    // MARK: - Outlets
    @IBOutlet weak var loginWebView: WKWebView!
    @IBOutlet weak var loginIndicator: UIActivityIndicatorView!
    // MARK: - Variables
    var arrayInstagramImages = [String]()
    var newids = [String]()
    var instagramImages = [UIImage]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.loginWebView.uiDelegate = self
        self.loginWebView.navigationDelegate = self
        unSignedRequest()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    
    @IBAction func doneBtnClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: - unSignedRequest
    func  getImageFromUrl () {
       for url in self.arrayInstagramImages {
         let url = NSURL(string: url)
         let  data = NSData(contentsOf: url as! URL)
         let img = UIImage(data: data as!Data)
           self.instagramImages.append(img!)
        }
       }
    func unSignedRequest () {
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=code&scope=%@", arguments: [INSTAGRAM_IDS.INSTAGRAM_AUTHURL,INSTAGRAM_IDS.INSTAGRAM_CLIENT_ID,INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI, INSTAGRAM_IDS.INSTAGRAM_SCOPE ])
        let urlRequest =  URLRequest.init(url: URL.init(string: authURL)!)
        loginWebView.load(urlRequest)
    }
    
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        
        let requestURLString = (request.url?.absoluteString)! as String
        print(requestURLString)
        
        if requestURLString.hasPrefix(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI) {
            if let range: Range<String.Index> = requestURLString.range(of: "?code=")
            {
                //                handleAuth(authToken: requestURLString.substring(from: range.upperBound))
                let replaced = requestURLString.substring(from: range.upperBound).dropLast(2)
                print(replaced)
                let showImageVC = storyboard!.instantiateViewController(withIdentifier: "ShowImagesViewController") as! ShowImagesViewController
                                                   self.navigationController?.pushViewController(showImageVC, animated: true)
                showImageVC.recieveAccessTokenFromLastVC = String(replaced)
//getAccessToken(code: String(replaced))
                
               
            }
            
            return false;
        }
        return true
    }
    
    func getAccessToken(code: String){
        var access_token : String?
        var user_id : String?
        let _headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
        let params : Parameters =   [ "client_id":"\(INSTAGRAM_IDS.INSTAGRAM_CLIENT_ID)", "client_secret":"\(INSTAGRAM_IDS.INSTAGRAM_CLIENTSERCRET)", "grant_type":"authorization_code", "redirect_uri":"\(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI)", "code":"\(code)"]
        let url =  URL(string:"https://api.instagram.com/oauth/access_token" as String)
        
        AF.request(url!, method: .post, parameters: params, encoding: URLEncoding.httpBody , headers: _headers).responseJSON(completionHandler: {
            response in
            print("result----", response.value as Any)
            if response.value != nil{
                let jsonResponse = response.value as! NSDictionary
                
                if jsonResponse["access_token"] != nil
                {
                    access_token = String(describing: jsonResponse["access_token"]!)
                    print("access token.....\(access_token)")
                }
                
                if jsonResponse["user_id"] != nil{
                    user_id = String(describing: jsonResponse["user_id"]!)
                    print("user_id .........\(user_id)")
                }
                self.getMediaId(access_token: access_token!)
            }
            else if (response.error != nil){
                var error = response.error!.localizedDescription
                if error.contains("JSON could not") {
                    error = "Could not connect to server at the moment."
                }
            }
        })
    }
    
    func getMediaId(access_token: String){
           var mediaId : String?
           var caption : String?
           let _headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
           let url =  URL(string:"https://graph.instagram.com/me/media?fields=id,caption&access_token=\(access_token)" as String)
           
        AF.request(url!, method: .get, parameters: nil, encoding: URLEncoding.httpBody , headers: _headers).responseJSON(completionHandler: {
               response in
                if response.value != nil{
               print("result----", response.value as Any)
                    let fetchedData = response.value as! [String:Any]
                    print(fetchedData["data"])
                    if let getids = fetchedData["data"] as? [[String:Any]] {
                        for i in getids {
                            if let ids = i["id"] as? String {
                                self.newids.append(ids)
                                self.getImagesUsingId(access_token: access_token, id: ids)
                            }
                        }
                        print(self.newids)
                    }
               }
               else if (response.error != nil){
                   var error = response.error!.localizedDescription
                   if error.contains("JSON could not") {
                       error = "Could not connect to server at the moment."
                   }
               }
           })
       }
    func getImagesUsingId(access_token : String,id : String){
              var imageid : String?
              var imageUrl : String?
              let _headers : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded"]
              let url =  URL(string:"https://graph.instagram.com/\(id)?fields=id,media_type,media_url,username,timestamp&access_token=\(access_token)" as String)
              AF.request(url!, method: .get, parameters: nil, encoding: URLEncoding.httpBody , headers: _headers).responseJSON(completionHandler: {
                  response in
                  print("result----", response.value as Any)
                  if response.value != nil{
                      let jsonResponse = response.value as! NSDictionary
                      
                      if jsonResponse["id"] != nil
                      {
                          imageid = String(describing: jsonResponse["id"]!)
                          print("imageid.....\(imageid)")
                      }
                      
                      if jsonResponse["media_url"] != nil{
                          imageUrl = String(describing: jsonResponse["media_url"]!)
                          print("imageurl .........\(imageUrl)")
                        self.arrayInstagramImages.append(imageUrl!)
                        //self.getImageFromUrl()
                      }
                  }
                  else if (response.error != nil){
                      var error = response.error!.localizedDescription
                      if error.contains("JSON could not") {
                          error = "Could not connect to server at the moment."
                      }
                  }
              })
        
    }
    
    func handleAuth(authToken: String)  {
        print("Instagram authentication token ==", authToken)
        //        let request = NSMutableURLRequest(url: URL(string:"https://api.instagram.com/v1/users/self/media/recent/?access_token=\(accessToken)")!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 120)
        
        self.fetchPhotosFromInstagramWithAccessToken(url: "https://api.instagram.com/v1/users/self/media/recent/?access_token=\(authToken)")
        UserDefaults.standard.setValue(true, forKey: "isLoggedInToInstagramKodak")
        UserDefaults.standard.setValue(authToken, forKey: "instagramAccessToken")
        
        let url = URL(string: "https://api.instagram.com/v1/users/self/?access_token=\(authToken)")
        AF.request(url!,method: .get, parameters:nil, encoding: JSONEncoding.default, headers: nil).responseJSON
            { response in
                
                print(response)
                switch(response.result)
                {
                case .success(_):
                    if response.value != nil
                    {
                        let dict = response.value! as! [String:Any]
                        let response = dict["data"] as! [String:Any]
                        let userName = response["username"] as! String
                        UserDefaults.standard.setValue(userName, forKey: "instagramUserName")
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "instagramUserChanged"), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name("reloadSocialMedia"), object: nil)
                    }
                    
                    break
                    
                case .failure(let error):
                    
                    var error = error.localizedDescription
                    
                    if error.contains("JSON could not") {
                        error = "Could not connect to server at the moment."
                    }
                }
        }
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "instagramUserChanged"), object: nil)
        
        
    }
    
    
    // MARK: - UIWebViewDelegate
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        loginIndicator.isHidden = false
        loginIndicator.startAnimating()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finished launching")
        loginIndicator.isHidden = true
        loginIndicator.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        //        print("BODY", navigationResponse.response)
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        //        print("BODY", navigationAction.request)
        self.checkRequestForCallbackURL(request: navigationAction.request)
        decisionHandler(.allow)
//        let vc = storyboard!.instantiateViewController(withIdentifier: "CollectionViewController") as! CollectionViewController
//                            self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        print("redirect")
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool
    {
        return checkRequestForCallbackURL(request: request)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        loginIndicator.isHidden = false
        loginIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        loginIndicator.isHidden = true
        loginIndicator.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        webViewDidFinishLoad(webView)
    }
    
    
    
    func fetchPhotosFromInstagramWithAccessToken(url: String) -> Void
    {
        let request = NSMutableURLRequest(url: URL(string:url)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 120)
        
        request.httpMethod = "GET" // POST ,GET, PUT What you want
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest) {data,response,error in
            //Akshay Solve crash detect on crashanalitics
            if data != nil
            {
                
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    {
                        //print("jsonResultData instagram photo : \(jsonResult)")
                        
                        //Akshay Solve crash detect on crashanalitics Add try catch
                        
                        if let jsonResultData = jsonResult["data"] as? [[String:Any]]
                        {
                            
                            
                            print(jsonResult["pagination"])
                            
                            for jsonDict in jsonResultData
                            {
                                if jsonDict["type"] as! String == "image"
                                {
                                    let imagedict = jsonDict["images"] as! [String:Any]
                                    let imageresolutiondict = imagedict["standard_resolution"] as!  [String:Any]
                                    let stringURL = imageresolutiondict["url"] as! String
                                    //self.arrayInstagramImages.append(stringURL)
                                }
                                else if jsonDict["type"] as! String == "carousel"
                                {
                                    let imageArray = jsonDict["carousel_media"] as! [[String:Any]]
                                    for object in imageArray
                                    {
                                        //Akshay Solve crash detect on crashanalitics
                                        if let imagedict = object["images"] as? [String:Any]
                                        {
                                            //let imagedict = object["images"] as! [String:Any]
                                            let imageresolutiondict = imagedict[ "standard_resolution"] as! [String:Any]
                                            let stringURL = imageresolutiondict["url"] as! String
//                                            self.arrayInstagramImages.append(stringURL)
                                        }
                                        
                                    }
                                }
                            }
                        }
                        
                        //Akshay Solve crash detect on crashanalitics
                        if let pagination = jsonResult["pagination"] as? [String:Any]
                        {
                            
                            
                            if (jsonResult["pagination"] as! [String:Any]).count > 0
                            {
                                let pageDict = jsonResult["pagination"] as! [String:Any]
                                let next_url = pageDict["next_url"] as? String
                                if let url2 = next_url
                                {
                                    self.fetchPhotosFromInstagramWithAccessToken(url: url2)
                                }
                            }
                                
                            else
                            {
                                DispatchQueue.main.async {
                                    self.saveImagesInDBAndMoveToNext()
                                }
                            }
                            
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
                
            }
        }
        dataTask.resume()
    }
    
    func saveImagesInDBAndMoveToNext()
    {
        self.navigationController?.popViewController(animated: true)
        
        if self.arrayInstagramImages.count > 0
        {
            // DBLayer().insertInstagramPhotos(urlArray: self.arrayInstagramImages)
            print("Array Instagram ", self.arrayInstagramImages.count)
        }
        else
        {
            /*
             let alert = UIAlertController(title: NSLocalizedString("Alert".localisedString(), comment:""), message: NSLocalizedString("Could not fetch images".localisedString(), comment:""), preferredStyle: UIAlertControllerStyle.alert)
             alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
             self.present(alert, animated: true, completion: nil)*/
        }
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "instagramUserChanged"), object: nil)
    }
}
