//
//  CollectionViewController.swift
//  PrateekMVVMLoginDemo
//
//  Created by maximess206 on 26/03/20.
//  Copyright © 2020 maximess206. All rights reserved.
//

import UIKit

class CollectionViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func moveToInstaVC(_ sender: UIButton) {
                let vc = storyboard!.instantiateViewController(withIdentifier: "InstagramViewController") as! InstagramViewController
                                    self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
