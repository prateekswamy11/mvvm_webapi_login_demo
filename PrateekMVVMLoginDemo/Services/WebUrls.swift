//
//  WebUrls.swift
//  PrateekMVVMLoginDemo
//
//  Created by maximess206 on 20/03/20.
//  Copyright © 2020 maximess206. All rights reserved.
//

import Foundation
enum WebUrl{
    enum Environment{
      static let QA = ""
      static let Dev = "http://142.93.210.133:5043/"
      static let live = ""
    }
    static let baseUrl: String = Environment.Dev
    static let register: String = "register"
    static let login: String = "login"
}
