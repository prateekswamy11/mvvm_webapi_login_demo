//
//  WebServices.swift
//  PrateekMVVMLoginDemo
//
//  Created by maximess206 on 20/03/20.
//  Copyright © 2020 maximess206. All rights reserved.
//

import UIKit
import Alamofire
import SystemConfiguration

class WebserviceModelClass: NSObject {
    
    //MARK:- Variables
    static let shared = WebserviceModelClass()
    var responseDict = [String:Any]()
    //MARK: - Check Internet connection
    
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    func showAlertOn(error: String, completionHandler:@escaping (Bool) -> () = { _ in }) {
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            completionHandler(true)
        }))
        self.getCurrentViewController()!.present(alert, animated: true, completion: nil)
    }
    
    func getCurrentViewController() -> UIViewController? {
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            var currentController: UIViewController! = rootController
            while( currentController.presentedViewController != nil ) {
                currentController = currentController.presentedViewController
            }
            return currentController
        }
        return nil
    }
    
    
    //MARK: - Post method model function
    func postDataFor(module:String,subUrl:String,parameter:[String:Any], completionHandler:@escaping ((Bool,String,[String:Any])) -> ())
    {
        var appVersion = String()
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            appVersion = version
        }
        let extraPara : [String:Any] = ["build_app_version":appVersion,"app_version_number":1.0,"platform":"iOS"]
        let mainParamter = extraPara.reduce(parameter) { (partialResult , pair) in
            var partialResult = partialResult //without this line we could not modify the dictionary
            partialResult[pair.0] = pair.1
            return partialResult
        }
        let urlString = WebUrl.baseUrl + subUrl
        print("Main parameter:->\(mainParamter)")
        print("Main Url:->\(urlString)")
        let url = URL(string: urlString)
        AF.request(url!,method: .post, parameters: mainParamter, encoding: JSONEncoding.default, headers: nil).responseJSON
            { response in
                print(response)
                switch(response.result)
                {
                case .success(_):
                    print("result----", response.value as Any)
                    if response.value != nil
                    {
                        let dict = response.value! as! NSDictionary
                        print("Response New:-",dict)
                        guard let responseStatus = dict.value(forKey: "status") as? Bool else {
                            return
                        }
                        var successMsg = String()
                        if let msg = dict.value(forKey: "message") as? String
                        {
                            successMsg = msg
                        }
                        let data: [String: Any] = dict as! [String:Any]
                        self.responseDict = data["data"] as! [String : Any]
                        completionHandler((responseStatus,successMsg,self.responseDict))
                    }
                    break
                case .failure(let error):
                    var error = error.localizedDescription
                    if error.contains("JSON could not") {
                        error = "Could not connect to server at the moment."
                    }
                    completionHandler((false, error + " Please try again later.",self.responseDict))
                }
        }
    }
    
    
    //MARK: - Global function for get data
    
    func getDataFor(module:String,subUrl:String, completionHandler:@escaping ((Bool,String,[String:Any])) -> ())
    {
        let urlString = WebUrl.baseUrl + subUrl
        print("Main Url:->\(urlString)")
        let url = URL(string: urlString)
        AF.request(url!,method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON
            { response in
                print(response)
                //                switch(response.result)
                //                {
                //                case .success(_):
                //                    if response.result.value != nil
                //                    {
                //                        let dict = response.result.value! as! NSDictionary
                //
                //
                //                        var successMsg = "Success"
                //
                //
                //                        self.responseDict = dict as! [String:Any]
                //
                //
                //                        completionHandler((true,successMsg,self.responseDict))
                //
                //                    }
                //
                //                    break
                //
                //                case .failure(let error):
                //
                //                    var error = error.localizedDescription
                //
                //                    if error.contains("JSON could not") {
                //                        error = "Could not connect to server at the moment."
                //                    }
                //
                //                    completionHandler((false, error + " Please try again later.",self.responseDict))
                //                }
        }
    }
}
