//
//  ViewModel.swift
//  PrateekMVVMLoginDemo
//
//  Created by maximess206 on 20/03/20.
//  Copyright © 2020 maximess206. All rights reserved.
//

import Foundation
protocol ViewControllerDelegate {
    func getInfoBackEmail(isemailValid : Bool?)
    func getInfoBackPassword(ispasswordValid : Bool?)
    func sendStatus(isValid : Bool?)
}
class ViewModel : ViewModelDelegate{
    var delegate: ViewControllerDelegate?
    func sendValue(from emailTextField: String?, passwordTextField: String?) {
        let model = login()
        model.email = emailTextField
        model.password = passwordTextField
        if Validation.shared.validateEmailId(emailID: model.email!) == true && Validation.shared.validatePassword(password: model.password!) == true{
            let parameter:[String: String] = ["email": model.email!,
                                              "password": model.password!]
                   WebserviceModelClass.shared.postDataFor(module: "Login user", subUrl: WebUrl.login, parameter: parameter) { (isSuccess, message, data) in
                       if isSuccess {
                           print("data on correct input------", data)
                       }
                   }
              delegate?.sendStatus(isValid: true)
        } else if Validation.shared.validateEmailId(emailID: model.email!)
        {
            delegate?.getInfoBackEmail(isemailValid: true)
        } else {
            delegate?.getInfoBackEmail(isemailValid: false)
        }
        if Validation.shared.validatePassword(password: model.password!){
            delegate?.getInfoBackPassword(ispasswordValid: true)
        } else {
            delegate?.getInfoBackPassword(ispasswordValid: false)
        }
    }
}

