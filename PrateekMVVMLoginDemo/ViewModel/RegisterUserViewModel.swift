//
//  RegisterUserViewModel.swift
//  PrateekMVVMLoginDemo
//
//  Created by maximess206 on 20/03/20.
//  Copyright © 2020 maximess206. All rights reserved.
//

import Foundation
protocol ViewControllerDelegateRegisterUser {
    func getInfoBackEmail(isemailValid : Bool?)
    func getInfoBackPassword(ispasswordValid : Bool?)
    func getInfoBackName(isNameValid : Bool?)
     func getInfoBackDob(isDobEmpty : Bool?)
    func sendStatus(isValid : Bool?)
}
class ViewModelRegisterUser : ViewModelDelegateRegisterUser{
    var delegate: ViewControllerDelegateRegisterUser?
    func sendValue(nameTextField: String?, emailTextField: String?, passwordTextField: String?, dobTextField: String?) {
        let modelRegister = register()
        modelRegister.name = nameTextField
        modelRegister.email = emailTextField
        modelRegister.password = passwordTextField
        modelRegister.dob = dobTextField
        if Validation.shared.validateName(name: modelRegister.name!) == true && Validation.shared.validateEmailId(emailID: modelRegister.email!) == true && Validation.shared.validatePassword(password: modelRegister.password!) == true && !modelRegister.dob!.isEmpty{
            let parameter: [String: String] = ["name": modelRegister.name!,
                                               "password": modelRegister.password!,
                                               "email": modelRegister.email!,
                                               "dob": modelRegister.dob!]
                    WebserviceModelClass.shared.postDataFor(module: "Register user", subUrl: WebUrl.register, parameter: parameter) { (isSuccess, message, data) in
                        print("data------", data)
                        if isSuccess{
                            self.delegate?.sendStatus(isValid: true)
                            //self.showAlertOn(title: "Success", message: message)
                        } else{
                           // self.showAlertOn(title: "Error", message: message)
                            self.delegate?.sendStatus(isValid: false)
                        }
                    }
                      //delegate?.sendStatus(isValid: true)
        } else if Validation.shared.validateEmailId(emailID: modelRegister.email!)
                {
                    delegate?.getInfoBackEmail(isemailValid: true)
                } else {
                    delegate?.getInfoBackEmail(isemailValid: false)
                }
        if Validation.shared.validatePassword(password: modelRegister.password!){
                    delegate?.getInfoBackPassword(ispasswordValid: true)
                } else {
                    delegate?.getInfoBackPassword(ispasswordValid: false)
                }
        if Validation.shared.validateName(name: modelRegister.name!){
            delegate?.getInfoBackName(isNameValid: true)
        } else {
            delegate?.getInfoBackName(isNameValid: false)
        }
        if modelRegister.dob!.isEmpty{
            delegate?.getInfoBackDob(isDobEmpty: true)
        }
        
    }
}
